
Add the subgroup labels for content_multigroup.module in the first column when
the group is rendered as a table with multiple columns.

Only useful with the CCK-6.x-3.x multigroup


USAGE
————————————————————————————————————————————————————————————————————————————————

Enable the module and setup a multigroup in the field settings for a content
type.  The multigroup must have 2 or more repeats and at least one subgroup label
must exist.

Configure the multigroup with the options

  [x]  Multiple columns
       [x]  Print subgroup labels in first column

enabled for displaying subgroup labels in node edit forms.  In node display
fields settings, set the display format to "Table – Multiple columns" and set
the label for the subgroup to "First column".


LIMITATIONS
————————————————————————————————————————————————————————————————————————————————

When re-ordering the multigroup values in node edit form, the subgroup labels
are also moved.  This has of course no effect when saving, but it tricks the
user into believing so.
