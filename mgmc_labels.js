Drupal.behaviors.mgmcLabels = function() {
    $('input#edit-settings-multigroup-multiple-columns').addClass('mgmc-labels-processed').each(function() {
	var $mc = $(this);
	var $mgmcWrapper = $('#edit-settings-multigroup-mc-labels-wrapper');
	if ($mgmcWrapper.length != 1) {
	    return;
	}
	if (! $mc.attr('checked')) {
	    $mgmcWrapper.hide();
	}
	$mc.change(function(event) {
	    if ($mc.attr('checked')) {
		$mgmcWrapper.slideDown('fast');
	    } else {
		$mgmcWrapper.slideUp('fast');
	    }
	});
    });
};
