<?php

/**
 * @file
 * Form alter and theme override implementations related to multi column tables
 * in content_multigroup.module
 */

/**
 * Alternate theme for theme_content_multigroup_node_form()
 *
 * @see content_multigroup.module
 */
function theme_mgmc_labels_content_multigroup_node_form($element) {
  $group_name = $element['#group_name'];
  $groups = fieldgroup_groups($element['#type_name']);
  $group = $groups[$group_name];
  $group_multiple = $group['settings']['multigroup']['multiple'];
  $group_fields = $element['#group_fields'];

  $table_id = $element['#group_name'] .'_values';
  $table_class = 'content-multiple-table content-multigroup-edit-table-multiple-columns';
  $order_class = $element['#group_name'] .'-delta-order';
  $subgroup_settings = isset($group['settings']['multigroup']['subgroup']) ? $group['settings']['multigroup']['subgroup'] : array();
  $subgroup_labels = isset($group['settings']['multigroup']['labels']) ? $group['settings']['multigroup']['labels'] : array();

  $headers = array(array('data' => ''), array('data' => ''));
  foreach ($group_fields as $field_name => $field) {
    $required = !empty($field['required']) ? '&nbsp;<span class="form-required" title="'. t('This field is required.') .'">*</span>' : '';
    $headers[] = array(
      'data' => check_plain(t($field['widget']['label'])) . $required,
      'class' => 'content-multigroup-cell-'. str_replace('_', '-', $field_name),
    );
  }
  $rows = array();
  $i = 0;
  foreach (element_children($element) as $delta => $key) {
    if (is_numeric($key)) {
      $cells = array();
      $label = (!empty($subgroup_labels[$i]) ? theme('content_multigroup_node_label', check_plain(t($subgroup_labels[$i]))) : '');
      $element[$key]['_weight']['#attributes']['class'] = $order_class;

      $cells[] = array('data' => '', 'class' => 'content-multiple-drag');
      $delta_element = drupal_render($element[$key]['_weight']);
      if ($group_multiple == 1) {
        $remove_element = drupal_render($element[$key]['_remove']);
      }
      $cells[] = array(
        'data' => check_plain(t($subgroup_labels[$i])),
        'class' => 'content-multigroup-subgroup-label'
      );
      foreach ($group_fields as $field_name => $field) {
        $cell = array(
          'data' => (isset($element[$key][$field_name]) ? drupal_render($element[$key][$field_name]) : ''),
          'class' => 'content-multigroup-cell-'. str_replace('_', '-', $field_name),
        );
        if (!empty($cell['data']) && !empty($element[$key][$field_name]['#description'])) {
          $cell['title'] = $element[$key][$field_name]['#description'];
        }
        $cells[] = $cell;
      }

      $row_class = 'draggable';
      $cells[] = array('data' => $delta_element, 'class' => 'delta-order');
      if ($group_multiple == 1) {
        if (!empty($element[$key]['_remove']['#value'])) {
          $row_class .= ' content-multiple-removed-row';
        }
        $cells[] = array('data' => $remove_element, 'class' => 'content-multiple-remove-cell');
      }
      $rows[] = array('data' => $cells, 'class' => $row_class);
    }
    $i++;
  }

  drupal_add_css(drupal_get_path('module', 'content_multigroup') .'/content_multigroup.css');
  $output = theme('table', $headers, $rows, array('id' => $table_id, 'class' => $table_class));
  $output .= drupal_render($element[$group_name .'_add_more']);

  // Enable drag-n-drop only if the group really allows multiple values.
  if ($group_multiple >= 1) {
    drupal_add_tabledrag($table_id, 'order', 'sibling', $order_class);
    drupal_add_js(drupal_get_path('module', 'content') .'/js/content.node_form.js');
  }

  return $output;
}

/**
 * Alternate theme for theme_content_multigroup_display_table_multiple()
 *
 * @see content_multigroup.module
 */
function theme_mgmc_labels_content_multigroup_display_table_multiple($element) {
  $headers = array(array('data' => ''));
  foreach ($element['#fields'] as $field_name => $field) {
    $label_display = isset($field['display_settings']['label']['format']) ? $field['display_settings']['label']['format'] : 'above';
    $headers[] = array(
      'data' => ($label_display != 'hidden' ? check_plain(t($field['widget']['label'])) : ''),
      'class' => 'content-multigroup-cell-'. str_replace('_', '-', $field_name),
    );
  }
  $rows = array();
  foreach (element_children($element) as $delta) {
    $cells = array(
      array(
        'data' => check_plain(t($element['#subgroup_labels'][$delta])),
        'class' => 'content-multigroup-subgroup-label',
      )
    );
    $empty = TRUE;
    foreach ($element['#fields'] as $field_name => $field) {
      $item = drupal_render($element[$delta][$field_name]);
      $cells[] = array(
        'data' => $item,
        'class' => $element[$delta]['#attributes']['class'] .' content-multigroup-cell-'. str_replace('_', '-', $field_name),
      );
      if (!empty($item)) {
        $empty = FALSE;
      }
    }
    // Get the row only if there is at least one non-empty field.
    if (!$empty) {
      $rows[] = $cells;
    }
  }
  return count($rows) ? theme('table', $headers, $rows, $element['#attributes']) : '';
}

/**
 * Custom form alter implementation.
 */
function mgmc_labels_fieldgroup_group_edit_form(&$form, $form_state) {
  $type_name = $form['#content_type']['type'];
  $group_name = $form['group_name']['#default_value'];

  $content_type = content_types($type_name);
  $groups = fieldgroup_groups($type_name);
  $group = $groups[$group_name];

  if ($group['group_type'] != 'multigroup') {
    return;
  }
  $form['settings']['multigroup']['mc-labels'] = array(
    '#type' => 'checkbox',
    '#title' => t('Print subgroup labels in first column'),
    '#description' => "Enable this option to render the subgroup labels as the first column on the node edit form.  See below for labels for each subgroup.",
    '#default_value' => isset($group['settings']['multigroup']['mc-labels']) ? $group['settings']['multigroup']['mc-labels'] : 0,
    // FAPI increments elements with .001 for non-weighted elements within a
    // branch, which means 'multiple-columns' is .001 and 'required' is .002.
    // We want to be between these.
    '#weight' => .0015,
  );
  $me = drupal_get_path('module', 'mgmc_labels');
  drupal_add_js($me .'/mgmc_labels.js');
  drupal_add_css($me .'/mgmc_labels.css');
}

/**
 * Custom form alter implementation.
 */
function mgmc_labels_content_display_overview_form(&$form, $form_state) {
  $type_name = $form['#type_name'];
  $content_type = content_types($type_name);
  if (empty($content_type['fields'])) {
    return;
  }
  $contexts_selector = $form['#contexts'];
  $groups = fieldgroup_groups($type_name);
  $contexts = content_build_modes($contexts_selector);
  $label_options = array(
    'above' => t('Above'),
    'column' => t('First column'),
    'hidden' => t('<Hidden>'),
  );
  foreach ($groups as $group_name => $group) {
    if ($group['group_type'] != 'multigroup') {
      continue;
    }
    $subgroup_settings = isset($group['settings']['multigroup']['subgroup']) ? $group['settings']['multigroup']['subgroup'] : array();
    $subgroup_name = $group_name .'_subgroup';
    if (array_key_exists('label', $form[$subgroup_name])) {
      $form[$subgroup_name]['label']['#options'] = $label_options;
    }
  }
}
